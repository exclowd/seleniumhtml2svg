package com.company;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class Main {

    static void saveScreenShot(ChromeDriver driver, int no) throws IOException {
        WebElement ele = driver.findElement(By.id("main"));
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        BufferedImage fullImg = ImageIO.read(screenshot);
        Point pt = ele.getLocation();
        int w = ele.getSize().getWidth();
        int h = ele.getSize().getHeight() + 20;
        System.out.printf("%d %d %d %d", 0, 0, fullImg.getWidth(), h);
        BufferedImage subimage = fullImg.getSubimage(0, pt.getY(), fullImg.getWidth(), h);
        File outputfile = new File("./data/screenshot_" + no + ".png");
        ImageIO.write(subimage, "png", outputfile);
    }

    public static void main(String[] args) {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("test.txt"));
            String line = reader.readLine();
            int i = 0;
            while (line != null) {
                String html_header = "<!DOCTYPE html><html><head><script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML\"></script></head><body><div id=\"main\">";
                String html_footer = "</div></body></html>";
                String content = html_header + line + html_footer;
                FileWriter fileWriter = new FileWriter("./out.html");
                fileWriter.write(content);
                fileWriter.close();
                line = reader.readLine();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("--headless");
                ChromeDriver driver = new ChromeDriver(options);
                driver.get("file:///home/vagrant/yschool/java/out.html");
                saveScreenShot(driver, i++);
                driver.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}